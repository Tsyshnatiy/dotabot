#include "MyStrategy.h"

#include <Core/inc/Creators.h>
#include <Core/inc/AI.h>
#include <Core/inc/ISpellAttackComputer.h>
#include <Core/inc/ITargetComputer.h>
#include <Core/inc/ILevelUpStrategy.h>
#include <Core/inc/INavigationManager.h>
#include <Core/inc/IUnitsStrengthComputer.h>

#include "Multimedia/SoundManager.h"

MyStrategy::MyStrategy()
  : m_pAi(nullptr),
  m_pSoundManager(new SoundManager())
{
  auto pUnitsStrengthComputerForSpells = core::createUnitStrengthComputer();
  auto pSpellAttackComputer = core::createSpellAttackComputer(std::move(pUnitsStrengthComputerForSpells));
  auto pTargetComputer = core::createTargetComputer(std::move(pSpellAttackComputer));
  auto pLevelUpStrategy = core::createLevelUpStrategy();
  auto pNavigationManager = core::createNavigationManager();
  auto pUnitsStrengthComputerForAi = core::createUnitStrengthComputer();

  m_pAi.reset(new core::AI(
    std::move(pTargetComputer),
    std::move(pLevelUpStrategy),
    std::move(pNavigationManager),
    std::move(pUnitsStrengthComputerForAi)));

  m_pAi->registerObserver(m_pSoundManager);
}

MyStrategy::~MyStrategy()
{
}

void MyStrategy::move(const model::Wizard& self, const model::World& world, const model::Game& game, model::Move& move)
{
  m_pAi->initialize(self, world, game, move);
  m_pAi->compute();
}