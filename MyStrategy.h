#pragma once

#include "Strategy.h"

namespace core
{
  class AI;
}

class SoundManager;

class MyStrategy : public Strategy
{
public:
  MyStrategy();
	~MyStrategy();

  void move(const model::Wizard& self, const model::World& world, const model::Game& game, model::Move& move) override;

private:
  std::unique_ptr<core::AI> m_pAi;
  std::shared_ptr<SoundManager> m_pSoundManager;
};
