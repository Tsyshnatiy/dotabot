#include "pch.h"
#include <iostream>


#include <optional>
#include <assert.h>

#include "TargetWeightComputer.h"
#include "Minion.h"
#include "Wizard.h"
#include "Building.h"

using namespace model;
using namespace core;

namespace
{
  Wizard createSelf()
  {
    return Wizard(0, 500, 500, 0, 0, 0, Faction::FACTION_ACADEMY, 30, 100, 100,
      {}, 0, true, 100, 100, 600, 600, 0, 0, {}, 0,
      {}, false, {});
  }
}

int main()
{
  const auto me = createSelf();
  Minion m1(1, 600, 600, 0, 0, 0, Faction::FACTION_RENEGADES, 30, 10, 100, {}, MinionType::MINION_ORC_WOODCUTTER, 500, 10, 0, 0);
  Minion m2(2, 1010, 1010, 0, 0, 0, Faction::FACTION_RENEGADES, 30, 10, 100, {}, MinionType::MINION_FETISH_BLOWDART, 500, 10, 0, 0);

  const auto m1W = TargetWeightComputer::weight(&me, &m1, UnitType::Minion);
  const auto m2W = TargetWeightComputer::weight(&me, &m2, UnitType::Minion);

  assert(m1W > m2W);
}
