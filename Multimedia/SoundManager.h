#pragma once

#include <Core/inc/AI.h>

class SoundManager : public core::AI::IObserver
{
public:
  void onFireball() override;
};