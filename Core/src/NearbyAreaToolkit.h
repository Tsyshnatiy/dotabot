#pragma once

#include <vector>

#include <Faction.h>

#include "Types.h"
#include "Point2d.h"

namespace core
{
  struct Units
  {
    std::vector<MinionPtr> minions;
    std::vector<WizardPtr> wizards;
    std::vector<BuildingPtr> buildings;

    bool empty() const;
  };

  Units findUnits(model::Faction faction, const Point2D& pos,
                  const double radius, const::model::World* pWorld);
}