#include "LevelUpStrategy.h"

#include <Wizard.h>
#include <Move.h>

using namespace core;

LevelUpStrategy::LevelUpStrategy()
{
  m_skillsToLearn.push(model::SkillType::SKILL_MAGICAL_DAMAGE_BONUS_PASSIVE_1);
  m_skillsToLearn.push(model::SkillType::SKILL_MAGICAL_DAMAGE_BONUS_AURA_1);
  m_skillsToLearn.push(model::SkillType::SKILL_MAGICAL_DAMAGE_BONUS_PASSIVE_2);
  m_skillsToLearn.push(model::SkillType::SKILL_MAGICAL_DAMAGE_BONUS_AURA_2);
  m_skillsToLearn.push(model::SkillType::SKILL_FROST_BOLT);

  m_skillsToLearn.push(model::SkillType::SKILL_STAFF_DAMAGE_BONUS_PASSIVE_1);
  m_skillsToLearn.push(model::SkillType::SKILL_STAFF_DAMAGE_BONUS_AURA_1);
  m_skillsToLearn.push(model::SkillType::SKILL_STAFF_DAMAGE_BONUS_PASSIVE_2);
  m_skillsToLearn.push(model::SkillType::SKILL_STAFF_DAMAGE_BONUS_AURA_2);
  m_skillsToLearn.push(model::SkillType::SKILL_FIREBALL);
}

void LevelUpStrategy::tryLevelUp(const model::Wizard* pWizard, model::Move* pMove)
{
  const auto& skills = pWizard->getSkills();
  if (skills.size() >= pWizard->getLevel() || m_skillsToLearn.empty())
    return;

  const auto skill = m_skillsToLearn.front();
  pMove->setSkillToLearn(skill);

  m_skillsToLearn.pop();
}