#include "SpellAttackComputer.h"

#include <algorithm>
#include <map>

#include <Wizard.h>
#include <Game.h>
#include <Building.h>
#include <Minion.h>

#include "NearbyAreaToolkit.h"
#include "IUnitsStrengthComputer.h"

using namespace core;

namespace
{
  bool hasSkill(const std::vector<model::SkillType>& skills, model::SkillType skill)
  {
    return std::find(skills.cbegin(), skills.cend(), skill) != skills.cend();
  }

  model::ActionType actionBySkill(model::SkillType skill)
  {
    static std::map<model::SkillType, model::ActionType> skillToAction
    {
      { model::SkillType::SKILL_FROST_BOLT, model::ActionType::ACTION_FROST_BOLT },
      { model::SkillType::SKILL_FIREBALL, model::ActionType::ACTION_FIREBALL },
    };

    return skillToAction[skill];
  }
}

SpellAttackComputer::SpellAttackComputer(std::unique_ptr<IUnitsStrengthComputer>&& pStrengthComputer)
  : m_pStrengthComputer(std::move(pStrengthComputer))
{

}

void SpellAttackComputer::initialize(const model::World* pWorld, const model::Game* pGame, const model::Wizard* pSelf)
{
  m_pWorld = pWorld;
  m_pGame = pGame;
  m_pSelf = pSelf;
}

bool SpellAttackComputer::canCast(model::SkillType skill, double manaCost) const
{
  if (!hasSkill(m_pSelf->getSkills(), skill))
    return false;

  if (m_pSelf->getMana() < manaCost)
    return false;

  const auto& actionCooldownTicks = m_pSelf->getRemainingCooldownTicksByAction();
  const auto action = actionBySkill(skill);
  const auto cooldownTicks = actionCooldownTicks[action];

  return cooldownTicks == 0;
}

LivingUnitPtr SpellAttackComputer::findFrostBoltTarget() const
{
  const auto isAcademy = m_pSelf->getFaction() == model::Faction::FACTION_ACADEMY;
  const auto foeFaction = isAcademy ? model::Faction::FACTION_RENEGADES : model::Faction::FACTION_ACADEMY;
  const Point2D myPos { m_pSelf->getX(), m_pSelf->getY() };
  const auto enemyUnits = findUnits(foeFaction, myPos, m_pSelf->getCastRange(), m_pWorld);

  const auto compare = [this](const auto& lhs, const auto& rhs)
  {
    return lhs->getLife() < rhs->getLife();
  };

  const auto wIt = std::min_element(enemyUnits.wizards.cbegin(), enemyUnits.wizards.cend(), compare);
  if (wIt == enemyUnits.wizards.cend())
    return nullptr;

  return *wIt;
}

LivingUnitPtr SpellAttackComputer::findFireballTarget() const
{
  const auto isAcademy = m_pSelf->getFaction() == model::Faction::FACTION_ACADEMY;
  const auto foeFaction = isAcademy ? model::Faction::FACTION_RENEGADES : model::Faction::FACTION_ACADEMY;
  const Point2D myPos{ m_pSelf->getX(), m_pSelf->getY() };
  const auto enemyUnits = findUnits(foeFaction, myPos, m_pSelf->getCastRange(), m_pWorld);

  if (!enemyUnits.wizards.empty())
    return enemyUnits.wizards.front();

  if (!enemyUnits.buildings.empty())
    return enemyUnits.buildings.front();

  if (!enemyUnits.minions.empty())
    return enemyUnits.minions.front();

  return nullptr;
}

std::shared_ptr<ISpellAttackComputer::Result> SpellAttackComputer::compute() const
{
  const auto findTargetForSkill = [this](double manacost,
                                          model::SkillType skill,
                                          auto targetFinder) -> std::shared_ptr<Result>
  {
    std::shared_ptr<Result> result(new Result);

    if (canCast(skill, manacost))
    {
      const auto oTarget = targetFinder();
      if (oTarget)
      {
        result->pUnit = oTarget;
        result->spell = actionBySkill(skill);
        return result;
      }
    }

    return nullptr;
  };

  auto pTarget = findTargetForSkill(m_pGame->getFrostBoltManacost(),
                                    model::SkillType::SKILL_FROST_BOLT,
                                    [this] { return findFrostBoltTarget(); });
  if (pTarget)
    return pTarget;

  pTarget = findTargetForSkill(m_pGame->getFireballManacost(),
                               model::SkillType::SKILL_FIREBALL,
                               [this] { return findFireballTarget(); });

  return pTarget;
}