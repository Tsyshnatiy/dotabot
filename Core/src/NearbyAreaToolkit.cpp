#include "NearbyAreaToolkit.h"

#include <World.h>
#include <Wizard.h>
#include <Minion.h>
#include <Building.h>

using namespace core;

bool Units::empty() const
{
  return minions.empty() && wizards.empty() && buildings.empty();
}

Units core::findUnits(model::Faction faction, const Point2D& pos,
                      const double radius, const::model::World* pWorld)
{
  const auto isInRadius = [radius, pos](const model::LivingUnit& unit)
  {
    return distance(unit, pos) <= radius;
  };
  const auto isFactionEqual = [faction](const model::LivingUnit& unit)
  {
    return faction == unit.getFaction();
  };

  Units result;

  for (const auto unit : pWorld->getBuildings())
  {
    if (isInRadius(unit) && isFactionEqual(unit))
    {
      auto pUnit = std::make_shared<model::Building>(unit);
      result.buildings.push_back(pUnit);
    }
  }

  for (const auto unit : pWorld->getMinions())
  {
    if (isInRadius(unit) && isFactionEqual(unit))
    {
      auto pUnit = std::make_shared<model::Minion>(unit);
      result.minions.push_back(pUnit);
    }
  }

  for (const auto unit : pWorld->getWizards())
  {
    if (isInRadius(unit) && isFactionEqual(unit))
    {
      auto pUnit = std::make_shared<model::Wizard>(unit);
      result.wizards.push_back(pUnit);
    }
  }

  return result;
}