#include "TargetWeightComputer.h"

#include <Minion.h>
#include <Wizard.h>

using namespace core;

double TargetWeightComputer::minionWeight(const model::Minion* pUnit)
{
  switch (pUnit->getType())
  {
  case model::MinionType::MINION_ORC_WOODCUTTER:
    return c_meleeMinionWeight;
  case model::MinionType::MINION_FETISH_BLOWDART:
    return c_rangedMinionWeight;
  }

  return 1;
}

double TargetWeightComputer::healthWeight(double hp, double maxHp)
{
  return maxHp / hp;
}

double TargetWeightComputer::distanceWeight(const model::LivingUnit* me, const model::LivingUnit* pUnit)
{
  return me->getDistanceTo(*pUnit) < c_criticalDistance ? 10 : 1;
}

double TargetWeightComputer::weight(const model::LivingUnit* me, const model::LivingUnit* pUnit, UnitType type)
{
  double result = 1;

  switch (type)
  {
  case UnitType::Building:
    result *= c_buildingWeight;
    break;
  case UnitType::Minion:
    result *= minionWeight(static_cast<const model::Minion*>(pUnit));
    break;
  case UnitType::Wizard:
    result *= c_wizardWeight;
    break;
  }

  result *= healthWeight(pUnit->getLife(), pUnit->getMaxLife());
  result *= distanceWeight(me, pUnit);

  return result;
}