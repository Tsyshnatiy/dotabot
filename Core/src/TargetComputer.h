#pragma once

#include "ITargetComputer.h"

namespace core
{
  class ISpellAttackComputer;

  class TargetComputer : public ITargetComputer
  {
  public:
    TargetComputer(std::unique_ptr<ISpellAttackComputer>&& pSpellAttackComputer);

    void initialize(const model::World*, const model::Game*, const model::Wizard*) override;

    Result compute() override;

  private:
    const model::World* m_pWorld = nullptr;
    const model::Game* m_pGame = nullptr;
    const model::Wizard* m_pSelf = nullptr;

    std::unique_ptr<ISpellAttackComputer> m_pSpellAttackComputer;
  };
}