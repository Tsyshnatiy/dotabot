#include "TargetComputer.h"

#include <queue>

#include <World.h>
#include <Game.h>
#include <Wizard.h>

#include "ISpellAttackComputer.h"
#include "TargetWeightComputer.h"

using namespace core;
using namespace model;

namespace
{
  struct Target
  {
    double weight = 0;
    LivingUnitPtr pUnit = nullptr;
  };

  struct TargetComparer
  {
    bool operator () (const Target& lhs, const Target& rhs)
    {
      return lhs.weight < rhs.weight;
    }
  };
}

TargetComputer::TargetComputer(std::unique_ptr<ISpellAttackComputer>&& pSpellAttackComputer)
  : m_pSpellAttackComputer(std::move(pSpellAttackComputer))
{

}

void TargetComputer::initialize(const model::World* pWorld,
                                const model::Game* pGame,
                                const model::Wizard* pWizard)
{
  m_pWorld = pWorld;
  m_pGame = pGame;
  m_pSelf = pWizard;

  m_pSpellAttackComputer->initialize(m_pWorld, m_pGame, m_pSelf);
}

ITargetComputer::Result TargetComputer::compute()
{
  const auto pSkillTarget = m_pSpellAttackComputer->compute();
  if (pSkillTarget)
  {
    Result result;
    result.pUnit = pSkillTarget->pUnit;
    result.attackType = pSkillTarget->spell;

    return result;
  }
  
  // TODO Staff attack?

  using Heap = std::priority_queue<Target, std::vector<Target>, TargetComparer>;

  const auto myFaction = m_pSelf->getFaction();

  TargetComparer comparer;
  Heap targets(comparer);

  const auto isEnemy = [myFaction](const model::LivingUnit* pUnit)
  {
    return pUnit->getFaction() != myFaction
      && pUnit->getFaction() != model::Faction::FACTION_NEUTRAL;
  };

  const auto isInRange = [self = m_pSelf](const model::LivingUnit* pUnit)
  {
    return self->getCastRange() >= self->getDistanceTo(*pUnit);
  };

  const auto fillQueue = [&targets, &isEnemy, &isInRange, self = m_pSelf](const auto& units, const UnitType type)
  {
    for (const auto& unit : units)
    {
      if (isEnemy(&unit) && isInRange(&unit))
      {
        Target target;
        target.pUnit = std::make_shared<model::LivingUnit>(unit);
        target.weight = TargetWeightComputer::weight(self, &unit, type);
        targets.push(target);
      }
    }
  };

  const auto& buildings = m_pWorld->getBuildings();
  const auto& minions = m_pWorld->getMinions();
  const auto& wizards = m_pWorld->getWizards();

  fillQueue(buildings, UnitType::Building);
  fillQueue(minions, UnitType::Minion);
  fillQueue(wizards, UnitType::Wizard);

  Result result{};

  if (targets.empty())
    return result;

  result.pUnit = targets.top().pUnit;
  result.attackType = model::ActionType::ACTION_MAGIC_MISSILE;

  return result;
}