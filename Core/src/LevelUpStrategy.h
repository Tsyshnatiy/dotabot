#pragma once

#include <queue>

#include <SkillType.h>

#include "ILevelUpStrategy.h"

namespace core
{
  class LevelUpStrategy : public ILevelUpStrategy
  {
  public:
    LevelUpStrategy();

    void tryLevelUp(const model::Wizard* pWizard, model::Move* pMove) override;

  private:
    std::queue<model::SkillType> m_skillsToLearn;
  };
}