#include "stdafx.h"

#include "AI.h"

#include <iostream>

#include <World.h>
#include <Game.h>
#include <Wizard.h>
#include <Move.h>

#include "ITargetComputer.h"
#include "ILevelUpStrategy.h"
#include "INavigationManager.h"
#include "IUnitsStrengthComputer.h"

using namespace core;

constexpr double c_low_hp_factor = 0.75;
constexpr int c_firstTicksCount = 500;
constexpr double c_standAndFightFactor = 2;

AI::AI(std::unique_ptr<ITargetComputer>&& pTargetComputer,
       std::unique_ptr<ILevelUpStrategy>&& pLevelUpStrategy,
       std::unique_ptr<INavigationManager>&& pNavigationManager,
       std::unique_ptr<IUnitsStrengthComputer>&& pUnitStrengthComputer)
  : m_pTargetComputer(std::move(pTargetComputer)),
  m_pLevelUpStrategy(std::move(pLevelUpStrategy)),
  m_pNavigationManager(std::move(pNavigationManager)),
  m_pUnitStrengthComputer(std::move(pUnitStrengthComputer))
{

}

void AI::registerObserver(std::shared_ptr<IObserver> pObserver)
{
  m_observers.push_back(pObserver);
}

void AI::notifyFireball()
{
  for (auto& pObserver : m_observers)
    pObserver->onFireball();
}

void AI::initialize(const model::Wizard& self,
  const model::World& world,
  const model::Game& game,
  model::Move& move)
{
  m_pWizard = &self;
  m_pWorld = &world;
  m_pGame = &game;
  m_pMove = &move;

  m_pTargetComputer->initialize(m_pWorld, m_pGame, m_pWizard);
  m_pNavigationManager->initialize(m_pWorld, m_pGame, m_pWizard, m_pMove);
  m_pUnitStrengthComputer->initialize(m_pWorld, m_pGame, m_pWizard);
}

bool AI::isTimeToFlee() const
{
  const auto alliedStrength = m_pUnitStrengthComputer->estimateNearbyAllies();
  const auto enemyStrength = m_pUnitStrengthComputer->estimateNearbyFoes();

  return (alliedStrength * c_standAndFightFactor) <= enemyStrength;
}

void AI::flee() const
{
  const auto wp = m_pNavigationManager->previousWaypoint();
  m_pNavigationManager->goTo(wp);

  m_pMove->setAction(model::ActionType::ACTION_STAFF);
}

void AI::compute()
{
  if (m_pWorld->getTickIndex() < c_firstTicksCount)
    return;

  m_pLevelUpStrategy->tryLevelUp(m_pWizard, m_pMove);

  const auto strafeSpd = m_pGame->getWizardStrafeSpeed();
  m_pMove->setStrafeSpeed(rd() % 2 ? strafeSpd : -strafeSpd);

  if (m_pWizard->getLife() < m_pWizard->getMaxLife() * c_low_hp_factor)
  {
    flee();
    return;
  }

  if (isTimeToFlee())
  {
    flee();
    return;
  }

  const auto target = m_pTargetComputer->compute();

  if (target.pUnit)
  {
    double distance = m_pWizard->getDistanceTo(*target.pUnit);

    if (distance <= m_pWizard->getCastRange())
    {
      double angle = m_pWizard->getAngleTo(*target.pUnit);

      m_pMove->setTurn(angle);

      if (std::fabs(angle) < m_pGame->getStaffSector() / 2.0)
      {
        if (target.attackType == model::ActionType::ACTION_FIREBALL)
          notifyFireball();

        m_pMove->setAction(target.attackType);
        m_pMove->setCastAngle(angle);
        m_pMove->setMinCastDistance(distance - target.pUnit->getRadius() + m_pGame->getMagicMissileRadius());
      }

      return;
    }
  }

  const auto wp = m_pNavigationManager->nextWaypoint();
  m_pNavigationManager->goTo(wp);
}