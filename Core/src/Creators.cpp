#include "Creators.h"

#include "TargetComputer.h"
#include "SpellAttackComputer.h"
#include "LevelUpStrategy.h"
#include "NavigationManager.h"
#include "UnitsStrengthComputer.h"

using namespace core;

std::unique_ptr<ISpellAttackComputer> core::createSpellAttackComputer(std::unique_ptr<IUnitsStrengthComputer>&& pComputer)
{
  return std::make_unique<SpellAttackComputer>(std::move(pComputer));
}

std::unique_ptr<ITargetComputer> core::createTargetComputer(std::unique_ptr<ISpellAttackComputer>&& pSpellAttackComputer)
{
  return std::make_unique<TargetComputer>(std::move(pSpellAttackComputer));
}

std::unique_ptr<ILevelUpStrategy> core::createLevelUpStrategy()
{
  return std::make_unique<LevelUpStrategy>();
}

std::unique_ptr<INavigationManager> core::createNavigationManager()
{
  return std::make_unique<NavigationManager>();
}

std::unique_ptr<IUnitsStrengthComputer> core::createUnitStrengthComputer()
{
  return std::make_unique<UnitsStrengthComputer>();
}