#include "Point2d.h"

#include <cmath>

#include <Unit.h>

using namespace core;
using namespace model;

double core::distance(const Point2D& p1, const Point2D& p2)
{
  return std::sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

double core::distance(const Point2D& p, const model::Unit& u)
{
  return distance(p, { u.getX(), u.getY() });
}

double core::distance(const model::Unit& u, const Point2D& p)
{
  return distance(p, u);
}