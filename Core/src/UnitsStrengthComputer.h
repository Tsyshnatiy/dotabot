#pragma once

#include <Faction.h>

#include "IUnitsStrengthComputer.h"

namespace core
{
  class UnitsStrengthComputer : public IUnitsStrengthComputer
  {
  public:
    void initialize(const model::World* pWorld,
      const model::Game* pGame,
      const model::Wizard* pSelf) override;

    double estimateNearbyAllies() const override;
    double estimateNearbyFoes() const override;

    double strength(const model::Wizard& w) const override;
    double strength(const model::Minion& w) const override;
    double strength(const model::Building& w) const override;

  private:
    double estimateNearbyUnitsStrength(model::Faction faction) const;

    const model::World* m_pWorld = nullptr;
    const model::Game* m_pGame = nullptr;
    const model::Wizard* m_pSelf = nullptr;
  };
}