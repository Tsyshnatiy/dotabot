#pragma once

#include <vector>

#include "INavigationManager.h"

namespace core
{
  class NavigationManager : public INavigationManager
  {
  public:
    void initialize(const model::World* pWorld,
      const model::Game* pGame,
      const model::Wizard* pSelf,
      model::Move* pMove) override;

    void goTo(const Point2D& pt) override;
    Point2D nextWaypoint() const override;
    Point2D previousWaypoint() const override;

  private:
    std::vector<Point2D> m_waypoints;

    const model::World* m_pWorld = nullptr;
    const model::Game* m_pGame = nullptr;
    const model::Wizard* m_pSelf = nullptr;
    model::Move* m_pMove = nullptr;
  };
}