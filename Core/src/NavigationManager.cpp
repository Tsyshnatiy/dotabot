#include "NavigationManager.h"

#include <Wizard.h>
#include <Game.h>
#include <World.h>
#include <Move.h>

using namespace core;
using namespace model;

constexpr double c_waypointRadius = 100;

void NavigationManager::initialize(const model::World* pWorld,
                                   const model::Game* pGame,
                                   const model::Wizard* pSelf,
                                   model::Move* pMove)
{
  m_pWorld = pWorld;
  m_pGame = pGame;
  m_pSelf = pSelf;
  m_pMove = pMove;

  const double mapSize = m_pGame->getMapSize();

  m_waypoints =
  {
    { 100.0, mapSize - 100.0 },
    { 600.0, mapSize - 200.0 },
    { 800.0, mapSize - 800.0 },
    { mapSize - 600.0, 600.0 }
  };
}

Point2D NavigationManager::nextWaypoint() const
{
  size_t lastWaypointIndex = m_waypoints.size() - 1;
  Point2D lastWaypoint = m_waypoints[lastWaypointIndex];

  for (size_t waypointIndex = 0; waypointIndex < lastWaypointIndex; ++waypointIndex)
  {
    Point2D waypoint = m_waypoints[waypointIndex];

    if (distance(waypoint, *m_pSelf) <= c_waypointRadius)
      return m_waypoints[waypointIndex + 1];

    if (distance(waypoint, lastWaypoint) < distance(lastWaypoint, *m_pSelf))
      return waypoint;
  }

  return lastWaypoint;
}

Point2D NavigationManager::previousWaypoint() const
{
  Point2D firstWaypoint = m_waypoints[0];

  for (size_t waypointIndex = m_waypoints.size() - 1; waypointIndex > 0; --waypointIndex)
  {
    Point2D waypoint = m_waypoints[waypointIndex];

    if (distance(waypoint, *m_pSelf) <= c_waypointRadius)
      return m_waypoints[waypointIndex - 1];

    if (distance(firstWaypoint, waypoint) < distance(firstWaypoint, *m_pSelf))
      return waypoint;
  }

  return firstWaypoint;
}

void NavigationManager::goTo(const Point2D& point)
{
  double angle = m_pSelf->getAngleTo(point.x, point.y);

  m_pMove->setTurn(angle);

  if (std::fabs(angle) < m_pGame->getStaffSector() / 4.0)
    m_pMove->setSpeed(m_pGame->getWizardForwardSpeed());
}