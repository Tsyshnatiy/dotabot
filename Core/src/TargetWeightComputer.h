#pragma once

#include "Types.h"

namespace model
{
  class Minion;
}

namespace core
{
  class TargetWeightComputer
  {
  public:
    static double weight(const model::LivingUnit* me, const model::LivingUnit* pUnit, UnitType type);

  private:
    static double minionWeight(const model::Minion* pUnit);
    static double distanceWeight(const model::LivingUnit* me, const model::LivingUnit* pUnit);
    static double healthWeight(double hp, double maxHp);

    static constexpr double c_buildingWeight = 100;
    static constexpr double c_wizardWeight = 50;
    static constexpr double c_rangedMinionWeight = 20;
    static constexpr double c_meleeMinionWeight = 20;

    static constexpr double c_criticalDistance = 300;
  };
}