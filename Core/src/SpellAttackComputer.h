#pragma once

#include <SkillType.h>

#include "Types.h"

#include "ISpellAttackComputer.h"

namespace core
{
  class IUnitsStrengthComputer;
  
  class SpellAttackComputer : public ISpellAttackComputer
  {
  public:
    SpellAttackComputer(std::unique_ptr<IUnitsStrengthComputer>&& pStrengthComputer);

    void initialize(const model::World* pWorld, const model::Game* pGame, const model::Wizard* pSelf) override;

    // Use optional here when upgrade visual studio
    std::shared_ptr<Result> compute() const override;

  private:
    bool canCast(model::SkillType, double manaCost) const;

    // Use optional here when upgrade visual studio
    LivingUnitPtr findFrostBoltTarget() const;
    LivingUnitPtr findFireballTarget() const;

    const model::World* m_pWorld = nullptr;
    const model::Game* m_pGame = nullptr;
    const model::Wizard* m_pSelf = nullptr;

    std::unique_ptr<IUnitsStrengthComputer> m_pStrengthComputer;
  };
}