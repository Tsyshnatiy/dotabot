#pragma once

#include <memory>

namespace model
{
  class World;
  class Game;
  class Wizard;
  class Move;

  class LivingUnit;
  class Minion;
  class Wizard;
  class Building;
}

namespace core
{
  using LivingUnitPtr = std::shared_ptr<model::LivingUnit>;
  using MinionPtr = std::shared_ptr<model::Minion>;
  using WizardPtr = std::shared_ptr<model::Wizard>;
  using BuildingPtr = std::shared_ptr<model::Building>;

  enum class UnitType
  {
    Building,
    Minion,
    Wizard,

    Count
  };
}