#pragma once

#include <vector>
#include <memory>
#include <random>

#include "Point2d.h"

namespace model
{
  class World;
  class Game;
  class Wizard;
  class Move;
  class LivingUnit;
}

namespace core
{
  class ILevelUpStrategy;
  class ITargetComputer;
  class INavigationManager;
  class IUnitsStrengthComputer;

  class AI
  {
  public:
    class IObserver
    {
    public:
      virtual ~IObserver() = default;

      virtual void onFireball() = 0;
    };

    AI(std::unique_ptr<ITargetComputer>&& pTargetComputer,
      std::unique_ptr<ILevelUpStrategy>&& pLevelUpStrategy,
      std::unique_ptr<INavigationManager>&& pNavigationManager,
      std::unique_ptr<IUnitsStrengthComputer>&& pUnitStrengthComputer);

    void initialize(const model::Wizard& self,
      const model::World& world,
      const model::Game& game,
      model::Move& move);

    void compute();

    void registerObserver(std::shared_ptr<IObserver> pObserver);

    void notifyFireball();

  private:
    bool isTimeToFlee() const;
    void flee() const;

    const model::Wizard* m_pWizard = nullptr;
    const model::World* m_pWorld = nullptr;
    const model::Game* m_pGame = nullptr;
    model::Move* m_pMove = nullptr;

    std::random_device rd;
    std::unique_ptr<ITargetComputer> m_pTargetComputer;
    std::unique_ptr<ILevelUpStrategy> m_pLevelUpStrategy;
    std::unique_ptr<INavigationManager> m_pNavigationManager;
    std::unique_ptr<IUnitsStrengthComputer> m_pUnitStrengthComputer;

    std::vector<std::shared_ptr<IObserver>> m_observers;
  };
}