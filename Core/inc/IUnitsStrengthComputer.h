#pragma once

#include "Types.h"

namespace model
{
  class Minion;
  class Wizard;
  class Building;
}

namespace core
{
  class IUnitsStrengthComputer
  {
  public:
    virtual ~IUnitsStrengthComputer() = default;

    virtual void initialize(const model::World* pWorld,
      const model::Game* pGame,
      const model::Wizard* pSelf) = 0;

    virtual double estimateNearbyAllies() const = 0;
    virtual double estimateNearbyFoes() const = 0;

    virtual double strength(const model::Wizard& w) const  = 0;
    virtual double strength(const model::Minion& w) const  = 0;
    virtual double strength(const model::Building& w) const = 0;
  };
}