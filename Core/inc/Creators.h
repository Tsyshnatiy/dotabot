#pragma once

#include <memory>

namespace core
{
  class ISpellAttackComputer;
  class ITargetComputer;
  class ILevelUpStrategy;
  class INavigationManager;
  class IUnitsStrengthComputer;

  std::unique_ptr<ILevelUpStrategy> createLevelUpStrategy();
  std::unique_ptr<ISpellAttackComputer> createSpellAttackComputer(std::unique_ptr<IUnitsStrengthComputer>&&);
  std::unique_ptr<ITargetComputer> createTargetComputer(std::unique_ptr<ISpellAttackComputer>&&);
  std::unique_ptr<INavigationManager> createNavigationManager();
  std::unique_ptr<IUnitsStrengthComputer> createUnitStrengthComputer();
}