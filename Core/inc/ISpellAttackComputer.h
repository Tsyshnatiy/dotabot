#pragma once

#include <ActionType.h>

#include "Point2d.h"
#include "Types.h"

namespace core
{
  class ISpellAttackComputer
  {
  public:
    struct Result
    {
      LivingUnitPtr pUnit;
      model::ActionType spell;
    };

    virtual ~ISpellAttackComputer() = default;

    virtual void initialize(const model::World* pWorld, const model::Game* pGame, const model::Wizard* pSelf) = 0;
    virtual std::shared_ptr<Result> compute() const = 0;
  };
}