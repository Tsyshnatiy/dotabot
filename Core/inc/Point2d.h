#pragma once

namespace model
{
  class Unit;
}

namespace core
{
  struct Point2D
  {
    double x = 0;
    double y = 0;
  };

  double distance(const Point2D& p1, const Point2D& p2);
  double distance(const Point2D& p, const model::Unit& u);
  double distance(const model::Unit& u, const Point2D& p);
}