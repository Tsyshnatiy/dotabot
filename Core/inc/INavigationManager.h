#pragma once

#include "Types.h"
#include "Point2d.h"

namespace core
{
  class INavigationManager
  {
  public:
    virtual ~INavigationManager() = default;

    virtual void initialize(const model::World* pWorld,
      const model::Game* pGame,
      const model::Wizard* pSelf,
      model::Move* pMove) = 0;

    virtual void goTo(const Point2D& pt) = 0;
    virtual Point2D nextWaypoint() const = 0;
    virtual Point2D previousWaypoint() const = 0;
  };
}