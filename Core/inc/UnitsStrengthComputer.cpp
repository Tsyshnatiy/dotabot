#include "UnitsStrengthComputer.h"

#include <Wizard.h>
#include <Minion.h>
#include <Building.h>

#include "NearbyAreaToolkit.h"

using namespace core;

constexpr double c_minionStrength = 20;
constexpr double c_towerStrength = 40;
constexpr double c_wizardStrength = 70;
constexpr double c_wizardStrengthPerLevel = 5;
constexpr double c_baseStrength = 70;

void UnitsStrengthComputer::initialize(const model::World* pWorld,
  const model::Game* pGame,
  const model::Wizard* pSelf)
{
  m_pWorld = pWorld;
  m_pGame = pGame;
  m_pSelf = pSelf;
}

double UnitsStrengthComputer::estimateNearbyAllies() const
{
  return estimateNearbyUnitsStrength(m_pSelf->getFaction());
}

double UnitsStrengthComputer::estimateNearbyFoes() const
{
  const auto isAcademy = m_pSelf->getFaction() == model::Faction::FACTION_ACADEMY;
  const auto foeFaction = isAcademy ? model::Faction::FACTION_RENEGADES : model::Faction::FACTION_ACADEMY;
  return estimateNearbyUnitsStrength(foeFaction);
}

double UnitsStrengthComputer::estimateNearbyUnitsStrength(model::Faction faction) const
{
  const auto units = findUnits(faction, { m_pSelf->getX(), m_pSelf->getY() }, m_pSelf->getVisionRange(), m_pWorld);

  double result = 0;

  for (const auto& pUnit : units.buildings)
    result += strength(*pUnit);

  for (const auto& pUnit : units.minions)
    result += strength(*pUnit);

  for (const auto& pUnit : units.wizards)
    result += strength(*pUnit);

  return result;
}
double UnitsStrengthComputer::strength(const model::Wizard& w) const
{
  const auto fullStrength = c_wizardStrength + c_wizardStrengthPerLevel * w.getLevel();
  const auto lifeFactor = w.getLife() / w.getMaxLife();
  const auto manaFactor = w.getMana() / w.getMaxMana();

  return fullStrength * lifeFactor * manaFactor;
}

double UnitsStrengthComputer::strength(const model::Minion& m) const
{
  const auto fullStrength = c_minionStrength;
  const auto lifeFactor = m.getLife() / m.getMaxLife();
  return fullStrength * lifeFactor;
}

double UnitsStrengthComputer::strength(const model::Building& b) const
{
  double fullStrength = 0;
  if (b.getType() == model::BuildingType::BUILDING_GUARDIAN_TOWER)
    fullStrength = c_towerStrength;
  else if (b.getType() == model::BuildingType::BUILDING_FACTION_BASE)
    fullStrength = c_baseStrength;

  const auto lifeFactor = b.getLife() / b.getMaxLife();
  return fullStrength * lifeFactor;
}