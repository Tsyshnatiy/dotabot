#pragma once

namespace model
{
  class Move;
  class Wizard;
}

namespace core
{
  class ILevelUpStrategy
  {
  public:
    virtual ~ILevelUpStrategy() = default;

    virtual void tryLevelUp(const model::Wizard* pWizard, model::Move* pMove) = 0;
  };
}