#pragma once

#include "Types.h"

#include <ActionType.h>

namespace model
{
  class World;
  class Game;
  class Wizard;
}

namespace core
{
  class ITargetComputer
  {
  public:
    // TODO MinCastDistance?
    struct Result
    {
      LivingUnitPtr pUnit;
      model::ActionType attackType = model::ActionType::_ACTION_UNKNOWN_;
    };

    virtual ~ITargetComputer() = default;

    virtual void initialize(const model::World*, const model::Game*, const model::Wizard*) = 0;

    virtual Result compute() = 0;
  };
}