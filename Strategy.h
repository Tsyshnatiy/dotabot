#pragma once

#ifndef _STRATEGY_H_
#define _STRATEGY_H_

#include <Game.h>
#include <Move.h>
#include <World.h>

class Strategy {
public:
    virtual void move(const model::Wizard& self, const model::World& world, const model::Game& game, model::Move& move) = 0;

    virtual ~Strategy();
};

#endif
